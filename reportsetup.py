import sys

from cx_Freeze import setup, Executable

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
        name = "Report",
        version = "1.1",
        description = "Report writer",
        executables = [Executable("report.py", base = base)])
