#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import time
import sys
import os.path
import traceback
try:
    import wx
except ImportError:
    raise ImportError,"The wxPython module is required to run this program"

POSITION_Y = 0
    
def timestamp_now(format):
    '''
    Make a timestamp with the current time using the string format supplied
    See http://docs.python.org/library/time.html#time.strftime for format
    '''
    return time.strftime(format, time.localtime(time.time()))

    
class simpleapp_wx(wx.Frame):
    def __init__(self, parent, id, title, pos):
        wx.Frame.__init__(self, parent, id, title, pos)
        self.parent = parent
        c_x, c_y, c_w, c_h = wx.ClientDisplayRect()
        self.win_width = c_w
        self.initialize()
        self.SetBackgroundColour(wx.WHITE)
        self._last_text = ''

    def initialize(self):
        sizer = wx.GridBagSizer()

        self.entry = wx.TextCtrl(self,-1, size=(self.win_width,0))
        sizer.Add(self.entry,(0,0),(1,1),wx.EXPAND)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnPressEnter, self.entry)
        self.Bind(wx.EVT_TEXT, self.OnKeyPressInTextField, self.entry)

        button = wx.Button(self,-1,label="Save")
        sizer.Add(button, (0,1))
        self.Bind(wx.EVT_BUTTON, self.OnButtonClick, button)

        self.label = wx.TextCtrl(self,-1, size=(self.win_width,20), style=(wx.TE_READONLY))
        self.label.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL))
        self.label.SetBackgroundColour(wx.WHITE)
        self.label.SetForegroundColour(wx.BLACK)
        sizer.Add( self.label, (1,0),(1,2), wx.EXPAND)

        sizer.AddGrowableCol(0)
        self.SetSizerAndFit(sizer)
        self.SetSizeHints(-1,self.GetSize().y,-1,self.GetSize().y );
        self.entry.SetFocus()
        self.entry.SetSelection(-1,-1)
        self.Show(True)

    def OnButtonClick(self,event):
        self._OnSave()

    def OnPressEnter(self,event):
        self._OnSave()
        
    def _OnSave(self):
        '''
        The GUI aspects of a save
        '''
        text = self.entry.GetValue()
        is_saved = self._save_entry(text)
        self.entry.SetFocus()
        self.entry.SetValue('')
        if is_saved:
            self.label.SetLabel(u"      Saved: " + text)
            self._last_text = text
        
    def OnKeyPressInTextField(self, event):
        num_char = len(self.entry.GetValue())
        # Present the number of letters typed in a string of exactly 10 characters
        num_char_str = str(num_char).rjust(4)
        self.label.SetLabel(u"%s  Saved: %s" % (num_char_str, self._last_text))
        
    def _save_entry(self, text):
        '''
        Non-GUI aspects of the save
        '''
        if text == '':
            return False
            
        try:
            filename = os.path.join(os.path.expanduser('~'), 'Documents', 'report.txt')
            reportfile = open(filename, 'a')
            reportfile.write(timestamp_now('%Y-%m-%d (%a) %H:%M:%S').encode('utf-8'))
            reportfile.write('\t'.encode('utf-8'))
            reportfile.write(text.encode('utf-8'))
            reportfile.write('\n'.encode('utf-8'))
        finally:
            reportfile.close()
        
        return True
            
if __name__ == "__main__":
    app = wx.App(redirect=True, filename="report.log")
    c_x, c_y, c_w, c_h = wx.ClientDisplayRect()
    position_x = c_x
    frame = simpleapp_wx(None,-1,'Report', (position_x, POSITION_Y))
    app.MainLoop()
